<?php
date_default_timezone_set('UTC');
$data = array();
$files = glob('data/*.json');
unlink('processed.json');
foreach($files as $file){
	$tweet = json_decode(file_get_contents($file));
	$d = new StdClass();

	if($tweet->user->statuses_count < 20 || $tweet->user->statuses_count > 1000){
		continue;
	}

	if(strpos($tweet->user->profile_image_url, 'sticky')){
		continue;
	}


	$d->text = $tweet->text;
	$d->user = new StdClass();
	$d->user->followers_count = $tweet->user->followers_count;
	$d->user->screen_name = $tweet->user->screen_name;
	$d->user->statuses_count = $tweet->user->statuses_count;
	$d->ratio = round(($tweet->user->statuses_count+1) / ($tweet->user->followers_count + 1));
	$d->date = strtotime($tweet->created_at);
	$data[] = $d;
	unset($tweet);
}

usort($data, function($a, $b){

	$a = $a->ratio;
	$b = $b->ratio;

	if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
});

$users = array();
foreach($data as $tweet){

	$users[$tweet->user->screen_name][] = ($tweet);
}

unset($data);

foreach($users as &$user){
	if(count($user) > 1){
		$var = tweet_variance($user);

		if($var < 75){
			unset($user);
		}
	}



}

file_put_contents('processed.json', json_encode($users));


function tweet_variance($user){

	$sims = array();
	if(count($user) == 1){
		$sims[] = 100;
	}
	foreach($user as $key=>$tweet){
		if(!isset($user[$key + 1])){
			continue;
		}
		similar_text($tweet->text, $user[$key+1]->text, $sim);
		
		$sims[] = $sim;
	}	


	return 100 - round(array_sum($sims) / count($sims), 2);

}
