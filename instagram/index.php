<?php
date_default_timezone_set('UTC');
$start = isset($_GET['p']) ? $_GET['p'] : 0;
$rpp = 400;

$start = $start * $rpp;


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
      <a class="navbar-brand" href="#">BBH Labs: Zero</a>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
          <ul class="nav navbar-nav">
            <li ><a href="/twitter">Twitter</a></li>
            <li class="active"><a href="/instagram">Instagram</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <?php
        
        $data = array();
        foreach(glob('data/users/*') as $file){
          $data[] = json_decode(file_get_contents($file));
        }

        usort($data, function($a, $b){
          $a = $a->counts->followed_by;
          $b = $b->counts->followed_by;
          
          if ($a == $b) {
              return 0;
          }
          return ($a > $b) ? 1 : -1;

        });

        $pages = count($data) / $rpp;

        echo render_pagination($pages);
        ?>
        <table class="table table-striped table-hover">
        <tr>
          <th></th>
          <th>Username</th>
          <th>Followers</th>
          <th>Follows</th>
          <th>Pics</th>
          <th>URL</th>
        </tr>
        
        <?php
        $data = array_splice($data, $start, $rpp);
          foreach($data as $key=>$d){


          //foreach($d as $tweet){
            echo '<tr>';
            echo "<td><img src='{$d->profile_picture}' height='100'/></td>";
            echo "<td>{$d->username}</td>";
            echo "<td>{$d->counts->followed_by}</td>";
            echo "<td>{$d->counts->follows}</td>";
            echo "<td>{$d->counts->media}</td>";
            echo "<td><a href='http://instagram.com/{$d->username}'>Clicky</a></td>";
            echo '</tr>';
          //}
        }

        ?>
        </table>
        <?php
        echo render_pagination($pages);
        ?>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php

function render_pagination($cnt){
  $cnt += 1; 
  $html = '<div class="row">
      <div class="col-md-10 col-md-offset-3"><ul class="pagination pagination-sm">';
  for($i = 1; $i <= $cnt; $i++){
    $html .= "<li><a href='?p={$i}'>{$i}</a></li>";
  }
  $html .= '</ul></div></div>';

  return $html;
}